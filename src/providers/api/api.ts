import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url: string = 'http://192.168.10.152/v1/api';

  constructor(public http: HttpClient) {
  }

	public _addStandardHeaders(header: HttpHeaders){
    header = header.set('Content-Type', 'application/json');
    header = header.set('Accept', 'application/json');
    return header;
  }

	public _initializeReqOpts(reqOpts){
    if (!reqOpts) {
      reqOpts = {
       headers: new HttpHeaders(),
       params: new HttpParams()
      };
    }
    return reqOpts;
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
		let data = '';
		for (let param of params){
				data += '/' + param
		}
    return this.http.get(this.url + '/' + endpoint + data, reqOpts);
  }

  post(endpoint: string, body: any) {
		let reqOpts;
     reqOpts = this._initializeReqOpts(reqOpts);
    reqOpts = this._addStandardHeaders(reqOpts.headers);
		console.log(reqOpts);
    return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }
}
