import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import * as Upsert from 'pouchdb-upsert';
import * as stream from 'pouchdb-replication-stream';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Pouch {
	private static readonly PROD_USER = "ofp.prieto@gmail.com";
	private static readonly PROD_REMOTE = 'https://bb41a9c3-d56d-4525-9dff-5f11cd7d0a70-bluemix.cloudantnosqldb.appdomain.cloud/';
	private static readonly PROD_PASS = "01z6y5AQ^hlFSX";
	private db: any;
	private personas: any;
	private remote: any;

  constructor() {
		PouchDB.plugin(Upsert);
		PouchDB.plugin(stream.plugin);
  }

	initDB(){

					this.db = new PouchDB('antenna-db', { adapter: 'websql' });
					this.personas = new PouchDB("personas", { adapter: 'websql' });

					let options: any = {
						live: true,
						retry: true,
						continuous: true,
						auth: {
							username: Pouch.PROD_USER,
							password: Pouch.PROD_PASS
						}
					};

					PouchDB.sync(this.personas, Pouch.PROD_REMOTE + '/', options)
						.on('error', (error) => {
							console.log(' public db init');
							console.error(error);
						});
	}

}
