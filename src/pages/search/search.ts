import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
currentItems: any = []

  constructor(public navCtrl: NavController, public navParams: NavParams, public items: Items) { 
		
	}

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.items.searchPerson([val])
		.subscribe(
			data => {
				console.log(data);
				this.currentItems = data;
			}, error => {
				console.error(error.status);
				console.error(error.error);
			}
		);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

}
