import { Component, ViewChild } from '@angular/core';
import { IonicPage, ModalController, NavController, Loading } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Item } from '../../models/item';
import { FormData } from '../../models/formData';
import { Items } from '../../providers/providers';
import { Nav } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-report-person',
  templateUrl: 'report-person.html',
})
export class ReportPersonPage {

	@ViewChild(Nav) nav: Nav;

  currentItems: Item[];
	estados = [];
	edad = [];
	todo: FormData = {'id':'', 
	'names':'', 
	'status': '1', 
	'age': '1', 
	'genre':'m', 
	'description':'',
	'rh':''
	};

	myForm: FormGroup;

  constructor(public navCtrl: NavController, 
	public items: Items, 
	public fb: FormBuilder,
	public loadingCtrl: LoadingController,
	public modalCtrl: ModalController,
	private alertCtrl: AlertController) {
		this.myForm = this.fb.group({
      names: ['', [Validators.required]],
      genre: ['', [Validators.required]],
      age: ['', [Validators.required]],
      id: ['', [Validators.required]],
      status: ['', [Validators.required]],
			description: ['', []]
    });

		this.estados = [
			{
			'id': "1", 
			'name': "Herido"
			},
			{
			'id': "2", 
			'name': "Fallecio"
			},
			{
			'id': "3", 
			'name': "Con vida"
			},
			{
			'id': "4", 
			'name': "Desaparecido"
			}
			];
			for (var _i = 0; _i < 99; _i++) {
    		this.edad.push(""+_i);
   
			}
  }

  ionViewDidLoad() {
  }

	logForm(){
		console.log("logFOrm");
		this.presentLoading();
		this.items.postPerson(JSON.stringify(this.myForm.value))
			.subscribe(
				data =>{
					console.log(data);
					this.dismissLoading();
					this.presentConfirm();
				}, error =>{
					this.dismissLoading();
					console.error(error.status);
					console.error(error.error);
				}
			);
		
	}


	presentConfirm() {
  let alert = this.alertCtrl.create({
    title: 'Reporte éxitoso',
    message: 'Reporte cargado!',
		enableBackdropDismiss: false,
    buttons: [
      {
        text: 'Aceptar',
        handler: () => {
          this.navCtrl.setRoot('TabsPage');
        }
      }
    ]
  });
  alert.present();
}
loader?: Loading;
presentLoading() {
     this.loader = this.loadingCtrl.create({
      content: "Enviando..."
    });
    this.loader.present();
  }

dismissLoading(){
	this.loader.dismiss();
}
	
}

