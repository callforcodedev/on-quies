 
export class FormData {
	'id': string = ''; 
	'type_doc'?: 'ti'|'cc'|'rc'|'e';
	'names': string = '';
	'status': '1'|'2'|'3'|'4'; 
	'age': string = '';
	'genre'?: "m"|"f"; 
	'description'?: string = '';
	'rh'?: string = '';
}